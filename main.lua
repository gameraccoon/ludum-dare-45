-----------------------------------
-- This file contains configurations of the game
-----------------------------------

-- После love.filesystem.setRequirePath("src/?.lua") не получается зареквайрить этот модуль
loveframes = require("src.loveframes")

love.filesystem.setRequirePath("src/?.lua")

require "utils.Tests"
require "utils.Debug"
require "core.World"
require "core.Game"
require "core.SoundManager"
require "core.Point"
require "core.Camera"

-- loves передаем в глобальный GUI, т.к. работа с интерфейсами будет вестить из сцен
require "gui"
GUI = loveframes

local gameName = "Cult War"
local gameId = "ludum_dare_cult_war_game"

soundManager = nil

love.filesystem.setIdentity(gameId)
love.window.setTitle(gameName)
if love.setDeprecationOutput then
	love.setDeprecationOutput(false) -- because we're supporting old love versions
end
love.window.setMode(1024, 600)

-- randomize random
math.randomseed(os.time())

-- enable for pixel perfect
--love.graphics.setDefaultFilter("nearest", "nearest", 0)

local canvasSize = MakePoint(love.graphics.getWidth(), love.graphics.getHeight())

function love.load()
	SetDebugMode(false)
	if IsDebugMode() then
		RunAllTests()
	end

	soundManager = MakeSoundManager()
	soundManager:LoadAll()

	game = MakeGame(canvasSize)
	game:Init()

	game:StartGame()
end

function love.update(dt)
	game:Update(dt)
	if not disableGuiInput then
		GUI:update(dt)
	end
end

function love.draw()
	game:GetCurrentScene():Draw()
	love.graphics.setBlendMode("alpha", "alphamultiply")
  	love.graphics.setColor(255, 255, 255, 255)
	GUI:draw()
	game:GetCurrentScene():PostGuiDraw()
end


function love.mousepressed(x, y, button)
    GUI.mousepressed(x, y, button)
end
 
function love.mousereleased(x, y, button)
    GUI.mousereleased(x, y, button)
end
 
function love.keypressed(key, unicode)
    GUI.keypressed(key, unicode)
    game:GetInputManager():OnSomethingPressed()
end
 
function love.keyreleased(key)
    GUI.keyreleased(key)
end
