return {
bgMusic={"music.ogg", volume=0.4, loop=true},

click={"click_ui.ogg", volume=0.15},
contactLong={"contact_long.ogg", volume=1},
contactShort={"contact_short.ogg", volume=0.5},

gameOver={"game_over.ogg", volume=1},
triumph={"triumph3.ogg", volume=1},
skillUpgrade={"skill_upgrade.ogg", volume=1},
}