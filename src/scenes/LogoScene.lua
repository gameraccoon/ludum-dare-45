require "core.Point"
require "core.Scene"

local function update(self, dt)
end

local function predraw(self)
	love.graphics.draw(self.texture, 0, 0)
end

local function postdraw(self)
end

local function init(self)
end

local function onopen(self)
end

function MakeLogoScene(game)
	local self = MakeScene(game)
	self:SetUpdateFn(update)
	self:SetPredrawFn(predraw)
	self:SetPostdrawFn(postdraw)
	self:SetInitFn(init)
	self:SetOnOpenFn(onopen)
	self.texture = love.graphics.newImage("data/textures/credits.png")
	self:GetInputManager():BindOnSomethingPressed(self, function()
		self:GetGame():SetScene("cardSelection")
	end, nil)
	return self
end
