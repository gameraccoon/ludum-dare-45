require "utils.Debug"
require "core.Point"
require "core.Scene"

local religions = {"Friendism", "Neomagnitism", "Mirrorism", "Alien Liberators", "Bee Church", "Cat Worshipers", "First Memologues", "Great Thinkers", "Hype Witnesses"}

local borderTexture = love.graphics.newImage("data/textures/border.png")
local font = love.graphics.newFont("data/fonts/JosefinSans-Regular.ttf", 16);
font2 = love.graphics.newFont("data/fonts/JosefinSans-Regular.ttf", 18);
local fontLadder = love.graphics.newFont("data/fonts/JosefinSans-Regular.ttf", 14);
local scoreboardBack = love.graphics.newImage("data/textures/gameplay/backboard.png")
local timerBack = love.graphics.newImage("data/textures/gameplay/timeline-without.png")
popupBack = love.graphics.newImage("data/textures/gameplay/BackTooltip.png")
local timerFillLeft = love.graphics.newImage("data/textures/gameplay/TimeLeft_Shadow.png")
local timerFillCenter = love.graphics.newImage("data/textures/gameplay/TimeMidle.png")
local timerFillRight = love.graphics.newImage("data/textures/gameplay/TimeLRight_Shadow.png")
local bgborder = love.graphics.newImage("data/textures/gameplay/screengrundg.png")

local function spawnRandomEnemy(self, blobType, name)
	local radius = nil
	if blobType == BlobTypeAiNeutral then
		if math.random(1, 100) < 50 then
			radius = math.random(self.levelData.neutralsSizeMin1, self.levelData.neutralsSizeMin2)*0.5
		elseif math.random(1, 100) < 90 then
			radius = math.random(self.levelData.neutralsSizeMin2, self.levelData.neutralsSizeMax1)*0.5
		else
			radius = math.random(self.levelData.neutralsSizeMax1, self.levelData.neutralsSizeMax2)*0.5
		end
	elseif blobType == BlobTypeAiBoss then
		blobType = BlobTypeAiActive
		radius = math.random(self.levelData.bossSizeMin, self.levelData.bossSizeMax)*0.5
		DebugLog("Boss is here!")
	else
		radius = math.random(self.levelData.religiousSizeMin, self.levelData.religiousSizeMax)*0.5
	end
	local spawnPos = MakePoint(math.random(radius, self.levelData.size.x - radius), math.random(radius, self.levelData.size.y - radius))

	self.world:AddActor(MakeBlob(self:GetGame(), spawnPos, radius, blobType, name, self.onBlobDied, self))
end

local function onWin(self)
	DebugLog("Level won")
	soundManager:SetVolume("bgMusic", 0.1)
	soundManager:PlaySound("triumph")
	self.waitForEndGame = true
	self.isWin = true
	self:GetGame():Pause()
end

local function onLoose(self)
	DebugLog("Level lost")
	soundManager:SetVolume("bgMusic", 0.1)
	soundManager:PlaySound("gameOver")
	self.waitForEndGame = true
	self.isWin = false
	self:GetGame():Pause()

	local playersAbove
	local blobs = self:GetWorld():GetAllActorsWithTag("blob")
	local biggerBlobs = 0
	for blob in pairs(blobs) do
		if blob.type == BlobTypeAiActive then
			if blob.radius > self.playerBlob.radius then
				biggerBlobs = biggerBlobs + 1
			end
		end
	end
	self.place = biggerBlobs + 1
	DebugLog("place: " .. self.place)
	local score = self:GetGame():GetParams().placeToScore[self.place] * self:GetGame():GetParams().multiplierFromLvl[self.levelData.levelId]
	if not score then score = 0 end
	DebugLog("score: " .. score.." lvl id: "..self.levelData.levelId )
	self.score = score
end

local function update(self, dt)
	if self.waitForEndGame then
		if self.afterGameTimer < 0 then
			if self.isWin then
				self:GetGame():WinLevel(self.levelData.levelId)
			else
				self:GetGame():LooseLevel(self.levelData.levelId, self.score)
			end
		else
			self.afterGameTimer = self.afterGameTimer - dt
		end
	elseif not self.waitForIntro then
		self.spawnTime = self.spawnTime + dt
		if self.spawnTime > self.levelData.neutralsSpawnPeriodSec then
			spawnRandomEnemy(self, BlobTypeAiNeutral, "")
			self.spawnTime = 0
		end

		self.levelTime = self.levelTime - dt
		if self.levelTime < 0 then
			DebugLog("Time is out")

			local biggestBlob = nil
			local blobs = self:GetWorld():GetAllActorsWithTag("blob")
			for blob in pairs(blobs) do
				if biggestBlob == nil then
					biggestBlob = blob
				else
					if blob.radius > biggestBlob.radius then
						biggestBlob = blob
					end
				end
			end

			if biggestBlob == nil or biggestBlob.type ~= BlobTypePlayer then
				onLoose(self)
			else
				onWin(self)
			end
		end
	end
end

local function predraw(self)
	local shift = self:GetCamera():GetDrawShift()
	local zoom = self:GetCamera():GetZoom()
	love.graphics.draw(borderTexture, -borderTexture:getWidth() + shift.x, -borderTexture:getHeight() + shift.y, 0, 1, (self.levelData.size.y * zoom + borderTexture:getWidth()) / borderTexture:getHeight())
	love.graphics.draw(borderTexture, shift.x, -borderTexture:getHeight() + shift.y, 0, self.levelData.size.x * zoom / borderTexture:getWidth(), 1)
	love.graphics.draw(borderTexture, -borderTexture:getWidth() + shift.x, self.levelData.size.y * zoom + shift.y, 0, (self.levelData.size.x * zoom + borderTexture:getWidth()) / borderTexture:getWidth(), 1)
	love.graphics.draw(borderTexture, self.levelData.size.x * zoom + shift.x, -borderTexture:getHeight() + shift.y, 0, 1, (self.levelData.size.y * zoom + borderTexture:getHeight() * 2) / borderTexture:getHeight())
end

local function drawScoreTable(self)
	local scoreboardPos = self:GetGame():GetParams().uiScoreboardPos

	love.graphics.draw(scoreboardBack, scoreboardPos.x, scoreboardPos.y)

	local textPos = scoreboardPos + MakePoint(25, 37)

	local blobs = self:GetWorld():GetAllActorsWithTag("blob")
	local blobScoreArray = {}
	for blob in pairs(blobs) do
		if blob.type ~= BlobTypeAiNeutral then
			table.insert(blobScoreArray, blob)
		end
	end

	table.sort(blobScoreArray, function (left, right)
		return left.radius > right.radius
	end)

	local yPosIndex = 1
	local wasPlayer = false
	local i2 = 1
	local ladderSize = 10
	for i, blob in pairs(blobScoreArray) do
		if i <= ladderSize or blob == self.playerBlob then
			local pos = textPos + MakePoint(0, yPosIndex * 17)
			local text = love.graphics.newText(fontLadder, i ..". " .. blob.name)-- .. " " .. math.floor(blob.radius))

			if blob == self.playerBlob then
				love.graphics.setColor(201/256, 196/256, 63/256, 255)
				love.graphics.draw(text, pos.x, pos.y)
				love.graphics.setColor(255, 255, 255, 255)
				wasPlayer = true
				yPosIndex = yPosIndex + 1
			elseif wasPlayer or i ~= ladderSize then
				love.graphics.setColor(209/256, 200/256, 191/256, 255)
				love.graphics.draw(text, pos.x, pos.y)
				love.graphics.setColor(255, 255, 255, 255)
				yPosIndex = yPosIndex + 1
			end
		end
		i2 = i
	end
	if not wasPlayer then
		local pos = textPos + MakePoint(0, yPosIndex * 17)
		local text = love.graphics.newText(fontLadder, (i2 + 1) ..". " .. self.playerBlob.name)
		love.graphics.setColor(201/256, 196/256, 63/256, 255)
		love.graphics.draw(text, pos.x, pos.y)
		love.graphics.setColor(255, 255, 255, 255)
	end
end

local function drawTimer(self)
	local timerPos = self:GetGame():GetParams().uiTimerPos:Clone()
	love.graphics.draw(timerBack, timerPos.x, timerPos.y)
	local scale = 2.87 * self.levelTime / self.timeFull
	if scale > 0 then
		timerPos.x = timerPos.x + 7
		timerPos.y = timerPos.y + 4
		love.graphics.draw(timerFillLeft, timerPos.x, timerPos.y)
		love.graphics.draw(timerFillCenter, timerPos.x + timerFillLeft:getWidth(), timerPos.y, 0, scale, 1)
		love.graphics.draw(timerFillRight, timerPos.x + timerFillLeft:getWidth() + timerFillCenter:getWidth()*scale, timerPos.y, 0, self.levelTime / self.timeFull, 1)
	end
end

local function drawPopup(self)
	if self.waitForEndGame then
		if not self.place then self.place = 1 end
		local ceneterTextPos = self:GetGame():GetParams().uiCenterTextPos

		love.graphics.draw(popupBack, ceneterTextPos.x - popupBack:getWidth()/2, ceneterTextPos.y)

		love.graphics.setColor(209/256, 200/256, 191/256, 255)
		local text = love.graphics.newText(font2, "Your position in the Cult war: ")
		love.graphics.draw(text, ceneterTextPos.x - text:getWidth()*0.5, ceneterTextPos.y + 45)
		local w = text:getWidth()*0.5
		love.graphics.setColor(201/256, 196/256, 63/256, 255)
		text = love.graphics.newText(font2, self.place)
		love.graphics.draw(text, ceneterTextPos.x + w, ceneterTextPos.y + 45)
		if not self.isWin then
			text = love.graphics.newText(font, "New cult followers: ")
			love.graphics.setColor(209/256, 200/256, 191/256, 255)
			love.graphics.draw(text, ceneterTextPos.x - text:getWidth()*0.5, ceneterTextPos.y + 74)
			w = text:getWidth()*0.5
			love.graphics.setColor(201/256, 196/256, 63/256, 255)
			text = love.graphics.newText(font, tostring(self.score))
			love.graphics.draw(text, ceneterTextPos.x + w, ceneterTextPos.y + 74)
		end
		love.graphics.setColor(255, 255, 255, 255)
	end
end

local function drawIntro(self)
	if self.waitForIntro then
		local ceneterTextPos = self:GetGame():GetParams().uiCenterTextPos

		love.graphics.draw(popupBack, ceneterTextPos.x - popupBack:getWidth()/2, ceneterTextPos.y)

		love.graphics.setColor(209/256, 200/256, 191/256, 255)
		local text1 = love.graphics.newText(font2, "Get first position before time runs out to extend your cult to this region")
		local text2 = love.graphics.newText(font2, "Capture those who are smaller. Beware of those who are bigger")
		local text3 = love.graphics.newText(font2, "Press WASD to move")
		
		love.graphics.draw(text1, ceneterTextPos.x - text1:getWidth()*0.5, ceneterTextPos.y + 35)
		love.graphics.draw(text2, ceneterTextPos.x - text2:getWidth()*0.5, ceneterTextPos.y + 60)
		love.graphics.draw(text3, ceneterTextPos.x - text3:getWidth()*0.5, ceneterTextPos.y + 85)
		love.graphics.setColor(255, 255, 255, 255)
	end
end

local function postdraw(self)
	love.graphics.draw(bgborder, 0, 0)
	drawScoreTable(self)
	drawTimer(self)
	drawPopup(self)
	drawIntro(self)
end

local function shuffle(array)
    -- fisher-yates
    local output = { }
    local random = math.random

    for index = 1, #array do
        local offset = index - 1
        local value = array[index]
        local randomIndex = offset*random()
        local flooredIndex = randomIndex - randomIndex%1

        if flooredIndex == offset then
            output[#output + 1] = value
        else
            output[#output + 1] = output[flooredIndex + 1]
            output[flooredIndex + 1] = value
        end
    end

    return output
end

local function populateBlobs(self)
	local religionIndexes = {}
	for i = 1, #religions do
		if i ~= selectedReligion then
			table.insert(religionIndexes, i)
		end
	end

	religionIndexes = shuffle(religionIndexes)

	for i = 1, self.levelData.religiousCount do
		spawnRandomEnemy(self, BlobTypeAiActive, religions[religionIndexes[i]])
	end

	for i = 1, self.levelData.neutralsInitialCount do
		spawnRandomEnemy(self, BlobTypeAiNeutral, "")
	end

	for i = 1, self.levelData.bossCount do
		spawnRandomEnemy(self, BlobTypeAiBoss, religions[religionIndexes[i + self.levelData.religiousCount]])
	end
end

local function init(self)
	self.input:BindOnMouseDown(self, function()
		if self:GetInputManager():IsInputEnabled() then
			local worldPos = self:GetCamera():ScreenToWorld(self.input:GetMousePos())

			-- mouse down
		end
	end)

	self.input:BindOnMouseMove(self, function(delta)
		if self:GetInputManager():IsInputEnabled() then
			local worldPos = self:GetCamera():ScreenToWorld(self.input:GetMousePos())

			-- mouse move
		end
	end)

	self.input:BindOnMouseUp(self, function()
		-- mouse up
	end)
end

local function startLevel(self, levelData)
	self.levelData = levelData
	self.levelTime = levelData.levelTimeSec
	self.timeFull = levelData.levelTimeSec

	self:GetCamera():SetBg({image=levelData.bg})

	local radius = self:GetGame():GetParams().heroInitialRadius * passiveAbilityFlockSize
	local spawnPos = MakePoint(math.random(radius, self.levelData.size.x - radius), math.random(radius, self.levelData.size.y - radius))
	self.playerBlob = MakeBlob(self:GetGame(), spawnPos, radius, BlobTypePlayer, religions[selectedReligion], self.onBlobDied, self)
	self.world:AddActor(self.playerBlob)

	populateBlobs(self)

	self.waitForIntro = true
	self:GetGame():Update(0.01)
	self:GetGame():Pause()
	self:GetInputManager():BindOnSomethingPressed(self, function()
		self.waitForIntro = false
		self:GetGame():Unpause()
		self:GetInputManager():UnBindOnSomethingPressed(self)
	end, nil)
end

local function getLevelData(self)
	return self.levelData
end

local function onBlobDied(self, blob)
	DebugLog("Someone have died")
	if blob.type == BlobTypePlayer then
		onLoose(self)
		return
	else
		if blob.type == BlobTypeAiActive then
			local blobs = self:GetWorld():GetAllActorsWithTag("blob")
			for blob in pairs(blobs) do
				if blob.type == BlobTypeAiActive then
					return
				end
			end
			-- no BlobTypeAiActive remained
			onWin(self)
		end
	end
end

function MakeGameplayScene(game)
	local self = MakeScene(game)
	self:SetUpdateFn(update)
	self:SetPredrawFn(predraw)
	self:SetPostdrawFn(postdraw)
	self:SetInitFn(init)
	self.StartLevel = startLevel
	self.GetLevelData = getLevelData
	self.levelData = {}
	self.spawnTime = 0
	self.afterGameTimer = game:GetParams().afterGameDelaySec
	self.timeFull = 0
	self.levelTime = 0
	self.waitForEndGame = false
	self.waitForIntro = false
	self.isWin = false
	self.score = 0
	self.onBlobDied = function(blob)
		onBlobDied(self, blob)
	end
	return self
end
