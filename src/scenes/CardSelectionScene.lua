require "core.Point"
require "core.Scene"
require "gui"
require "actors.Blob"

ReligionFriendism = 1
ReligionNeomagnetism = 2
ReligionMirrorism = 3

selectedReligion = 1

local function update(self, dt)
end

local function predraw(self)
end

local function postdraw(self)
end

local function init(self)
end

local function InitGUI(self)
	GUI_None()

	self.gui = {}

	local ignoreSound = true

	--фон
	local backImage = GUI.Create("image", back)
    backImage:SetImage("data/textures/cardselection/back.png")
    backImage:SetY(0)
    backImage:SetX(0)

    unselectAll = function()
    	self.gui.friendismImage.isSelected = false
	   	self.gui.friendismImage:SetImage("data/textures/cardselection/cards-passive 1.png")
	    self.gui.friendismImage:SetY(148)
	    self.gui.friendismImage:SetX(185) 


    	self.gui.neomagnetismImage.isSelected = false
	    self.gui.neomagnetismImage:SetImage("data/textures/cardselection/cards-passive 2.png")
	    self.gui.neomagnetismImage:SetY(148)
	    self.gui.neomagnetismImage:SetX(410) 

    	self.gui.mirrorismImage.isSelected = false
		self.gui.mirrorismImage:SetImage("data/textures/cardselection/cards-passive 3.png")
	    self.gui.mirrorismImage:SizeToImage()
	    self.gui.mirrorismImage:SetY(148)
	    self.gui.mirrorismImage:SetX(635) 
    end

	--карточка 1
    self.gui.friendismImage = GUI.Create("imagebutton")
    self.gui.friendismImage:SetImage("data/textures/cardselection/cards-passive 1.png")
    self.gui.friendismImage:SizeToImage()
    self.gui.friendismImage:SetY(148)
    self.gui.friendismImage:SetX(185) 
    self.gui.friendismImage:SetText("")
	self.gui.friendismImage.OnClick = function(object, x, y)
		if not self.gui.friendismImage.isSelected then
			if not ignoreSound then
				soundManager:PlaySound("click")
			end
			unselectAll()
			self.gui.friendismImage:SetImage("data/textures/cardselection/cards-active 1.png")
			self.gui.friendismImage:SetY(119)
			self.gui.friendismImage:SetX(161)
		end
		selectedReligion = ReligionFriendism
    end
	
	--карточка 2
	self.gui.neomagnetismImage = GUI.Create("imagebutton")
    self.gui.neomagnetismImage:SetImage("data/textures/cardselection/cards-passive 2.png")
    self.gui.neomagnetismImage:SizeToImage()
    self.gui.neomagnetismImage:SetY(148)
    self.gui.neomagnetismImage:SetX(410) 
    self.gui.neomagnetismImage:SetText("")
	self.gui.neomagnetismImage.OnClick = function(object, x, y)
		if not self.gui.neomagnetismImage.isSelected then
			if not ignoreSound then
				soundManager:PlaySound("click")
			end
			unselectAll()
			self.gui.neomagnetismImage:SetImage("data/textures/cardselection/cards-active 2.png")
			self.gui.neomagnetismImage:SetY(119)
			self.gui.neomagnetismImage:SetX(385)
		end
		selectedReligion = ReligionNeomagnetism
    end
	
	--карточка 3
	self.gui.mirrorismImage = GUI.Create("imagebutton")
    self.gui.mirrorismImage:SetImage("data/textures/cardselection/cards-passive 3.png")
    self.gui.mirrorismImage:SizeToImage()
    self.gui.mirrorismImage:SetY(148)
    self.gui.mirrorismImage:SetX(635) 
    self.gui.mirrorismImage:SetText("")
	self.gui.mirrorismImage.OnClick = function(object, x, y)
		if not self.gui.mirrorismImage.isSelected then
			if not ignoreSound then
				soundManager:PlaySound("click")
			end
			unselectAll()
			self.gui.mirrorismImage:SetImage("data/textures/cardselection/cards-active 3.png")
			self.gui.mirrorismImage:SetY(119)
			self.gui.mirrorismImage:SetX(610)
		end
		selectedReligion = ReligionMirrorism
    end
	
	--кнопка
    self.gui.buttonselect = GUI.Create("imagebutton", buttonselect)
    self.gui.buttonselect:SetImage("data/textures/cardselection/button-pass.png")
    self.gui.buttonselect:SizeToImage()
    self.gui.buttonselect:SetY(495)
    self.gui.buttonselect:SetX(380) 
    self.gui.buttonselect:SetText("")
    self.gui.buttonselect.isSelected = false
	self.gui.buttonselect.OnClick = function(object, x, y)
		soundManager:PlaySound("click")
		self.gui.buttonselect:SetImage("data/textures/cardselection/button-hover.png")
		self.gui.buttonselect:SetY(495)
		self.gui.buttonselect:SetX(380)

		self:GetGame():OpenMap()
	end
	
	self.gui.buttonselect.OnMouseEnter = function(object)
        object:SetImage("data/textures/cardselection/button-hover.png")
    end

	self.gui.buttonselect.OnMouseExit = function(object)
        object:SetImage("data/textures/cardselection/button-pass.png")
    end
	 
	
	--self.gui.panel = GUI.Create('panel')
	--self.gui.panel:Center()

	--self.gui.button = GUI.Create('button', panel)
	--self.gui.button:Center()

 	-- click the first card
    self.gui.friendismImage:OnClick(0, 0, true)
    ignoreSound = false
end

local function onopen(self)
	InitGUI(self)
end

function MakeCardSelectionScene(game)
	local self = MakeScene(game)
	self:SetUpdateFn(update)
	self:SetPredrawFn(predraw)
	self:SetPostdrawFn(postdraw)
	self:SetInitFn(init)
	self:SetOnOpenFn(onopen)
	return self
end
