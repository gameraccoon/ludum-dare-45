require "core.Point"
require "core.Scene"
require "data.LevelData"
require "data.Strings"
require "gui"
require "scenes.CardSelectionScene"

local globOffset = MakePoint(-40, 0)
local regionsConfig = {
	{ idName = '8', pos = globOffset, levelData = MakeLevelData('8')},
	{ idName = '7', pos = globOffset, levelData = MakeLevelData('7')},
	{ idName = '6', pos = globOffset, levelData = MakeLevelData('6')},
	{ idName = '5', pos = globOffset, levelData = MakeLevelData('5')},
	{ idName = '4', pos = globOffset, levelData = MakeLevelData('4')},
	{ idName = '3', pos = globOffset, levelData = MakeLevelData('3')},
	{ idName = '2', pos = globOffset, levelData = MakeLevelData('2')},
	{ idName = '1', pos = globOffset, levelData = MakeLevelData('1')}
}

local religionAbilTooltips = {}
religionAbilTooltips[ReligionFriendism] = love.graphics.newImage("data/textures/gui/tooltips/tooltip_Friendism.png")
religionAbilTooltips[ReligionNeomagnetism] = love.graphics.newImage("data/textures/gui/tooltips/tooltip_Neomagnitism.png")
religionAbilTooltips[ReligionMirrorism] = love.graphics.newImage("data/textures/gui/tooltips/tooltip_Mirrorism.png")

local religionTooltips = {}
religionTooltips[ReligionFriendism] = love.graphics.newImage("data/textures/gui/tooltips/tooltip_FriendismInfo.png")
religionTooltips[ReligionNeomagnetism] = love.graphics.newImage("data/textures/gui/tooltips/tooltip_NeomagnitismInfo.png")
religionTooltips[ReligionMirrorism] = love.graphics.newImage("data/textures/gui/tooltips/tooltip_MirrorismInfo.png")

local passiveTooltips = {}
passiveTooltips[1] = love.graphics.newImage("data/textures/gui/tooltips/tooltip_Flock.png")
passiveTooltips[2] = love.graphics.newImage("data/textures/gui/tooltips/tooltip_Influence.png")
passiveTooltips[3] = love.graphics.newImage("data/textures/gui/tooltips/tooltip_Spread.png")

local function update(self, dt)
end

local function predraw(self)
	love.graphics.draw(self.background, globOffset.x, globOffset.y)
end

local function drawFinishLevel(self)
	if self.waitForPopup then
		local ceneterTextPos = self:GetGame():GetParams().uiCenterTextPos

		love.graphics.draw(popupBack, ceneterTextPos.x - popupBack:getWidth()*0.5, ceneterTextPos.y)

		love.graphics.setColor(209/256, 200/256, 191/256, 255)
		love.graphics.printf(self.popupRawText, font2, math.floor(ceneterTextPos.x - self.popupText:getWidth()*0.5), math.floor(ceneterTextPos.y + 35), self.popupText:getWidth(), "center")
		love.graphics.setColor(255, 255, 255, 255)
	end
end

local function postdraw(self)
	local shift = self:GetCamera():GetDrawShift()
	for i = 1, #self.regions do
		local region = self.regions[i]
		region.postdraw(region, shift)
	end
end

function postguidraw(self)
	if self.currentTooltip then
		local tooltipPos = self:GetGame():GetInputManager():GetMousePos()
		if tooltipPos.x + self.currentTooltip:getWidth() > 1024 then
			tooltipPos.x = 1024 - self.currentTooltip:getWidth()
		end
		if tooltipPos.y + self.currentTooltip:getHeight() > 600 then
			tooltipPos.y = 600 - self.currentTooltip:getHeight()
		end
		love.graphics.draw(self.currentTooltip, tooltipPos.x, tooltipPos.y)
	end
	drawFinishLevel(self)
end

local function GetAbilityProgressByIndex(self, index)
	if self.selectedLevel ~= "" then
		local curLevelProgressData = self.game.levelsProgressData[self.selectedLevel]
		local curAbility = curLevelProgressData.abilityFlock
		if index == 2 then
			curAbility = curLevelProgressData.abilityInfluence
		elseif index == 3 then
			curAbility = curLevelProgressData.abilitySpread
		end
		DebugLog('Cur ability' .. curAbility)
		return curAbility
	end
	return 1
end

local function UpdateVariables(self)
	if self.gui and self.selectedLevel ~= "" then
		local curLevelProgressData = self.game.levelsProgressData[self.selectedLevel]
		if self.gui.scoreText then
			self.gui.scoreText:SetText({{color={1, 1, 0.4}}, tostring(curLevelProgressData.scores)})
		end
		if self.gui.abilities then
			for i=1, 3 do
				local curAbility = GetAbilityProgressByIndex(self, i)
				local ability = self.gui.abilities[i]
				local abilityCost = -1
				if curAbility <= 4 then
					abilityCost = self.game.costsToUpgrade[curAbility]
					ability.cost:SetText({{color={1, 1, 0.4}}, tostring(abilityCost)})
				end 
				for j=1, 4 do
					if j < curAbility then
						ability.levels[j]:SetImage("data/textures/gui/map/AbilityLvlUp.png")
					else
						ability.levels[j]:SetImage("data/textures/gui/map/AbilityLvlDis.png")
					end
				end
				
				if abilityCost > 0 and curLevelProgressData.scores >= abilityCost then
					ability.button:SetImage("data/textures/gui/map/buttonPlus.png")
				else
					ability.button:SetImage("data/textures/gui/map/buttonPlusDis.png")
				end
				
    			ability.button:SizeToImage()
			end
		end
	end
end

local function SelectLevel(self, levelId)
	self.selectedLevel = levelId
	for i = 1, #self.regions do
		local region = self.regions[i]
		region.selected = false
		if region.idName == self.selectedLevel then
			region.selected = true
			UpdateVariables(self)
		end
	end
end

function TryBuyUpgrade(self, index)
	if self.gui.abilities then
		local curLevelProgressData = self.game.levelsProgressData[self.selectedLevel]
		local curAbility = GetAbilityProgressByIndex(self, index)

		local abilityCost = -1
		if curAbility <= 4 then
			abilityCost = self.game.costsToUpgrade[curAbility]
		end
		if abilityCost > 0 and curLevelProgressData.scores >= abilityCost then
			curLevelProgressData.scores = curLevelProgressData.scores - abilityCost

			if index == 1 then
				curLevelProgressData.abilityFlock = curLevelProgressData.abilityFlock + 1
			elseif index == 2 then
				curLevelProgressData.abilityInfluence = curLevelProgressData.abilityInfluence + 1 
			elseif index == 3 then
				curLevelProgressData.abilitySpread = curLevelProgressData.abilitySpread + 1
			end
			soundManager:PlaySound("skillUpgrade")
			UpdateVariables(self)
		end
	end
end

local function showTooltip(self, tooltip)
	self.currentTooltip = tooltip
end

local function hideTooltip(self)
	self.currentTooltip = nil
end

local function initGUI(self)
	GUI_None()
	self.gui = {}

	self.gui.font = love.graphics.newFont("data/fonts/AbrilFatface-Regular.ttf", 20);
	self.gui.costFont = love.graphics.newFont("data/fonts/AbrilFatface-Regular.ttf", 14);

    local rightPanelWidth = 150
    local rightPanelHeight = love.graphics.getHeight()

    -- Создаем основную панель справа экрана
    -- Далее все размещаем на ней
	local panel = GUI.Create("image")
	panel:SetImage("data/textures/gui/map/Back_" .. selectedReligion .. '.png')
	panel:SetPos(0, 0)
	panelWidth = panel:GetWidth()
	
	self.gui.currency = GUI.Create("image", panel)
	self.gui.currency:SetImage("data/textures/gui/map/Currency.png")
	self.gui.currency:SetPos(panelWidth / 2 - self.gui.currency:GetWidth() / 2, 245)

	local ap_currency = GUI.Create("image", self.gui.currency)
	ap_currency:SetImage("data/textures/gui/map/ap_currency.png")
	ap_currency:SetPos(self.gui.currency:GetWidth() / 2 + 15, 245 + self.gui.currency:GetHeight() / 2 - 7)

	self.gui.scoreText = GUI.Create("text", self.gui.currency)
	self.gui.scoreText:SetPos(self.gui.currency:GetWidth() / 2 + 30, 245 + self.gui.currency:GetHeight() / 2 - 16)
	self.gui.scoreText:SetFont(self.gui.font)
	self.gui.scoreText:SetText({{color={1, 1, 0.4}}, "0"})
	
	local skills = GUI.Create("image", panel)
	skills:SetImage("data/textures/gui/map/Skills.png")
	skills:SetPos(panelWidth / 2 - skills:GetWidth() / 2, 310)

	self.gui.skills = {}
	for i=1, 5 do
		self.gui.skills[i] = GUI.Create("image", panel)
		if i == 1 then
			if selectedReligion == ReligionFriendism then 
				self.gui.skills[i]:SetImage("data/textures/gui/map/Friendismpassiveskill.png")
			elseif selectedReligion == ReligionNeomagnetism then 
				self.gui.skills[i]:SetImage("data/textures/gui/map/Neomagnitismpassiveskill.png")
			elseif selectedReligion == ReligionMirrorism then
				self.gui.skills[i]:SetImage("data/textures/gui/map/Mirrorismpassiveskill.png")
			end
		else
			self.gui.skills[i]:SetImage("data/textures/gui/map/Skill_Locked.png")
		end
		self.gui.skills[i]:SetPos(15 + 40 * (i-1), 350)
	end
	self.gui.skills[1].OnMouseEnter = function(object)
        showTooltip(self, religionAbilTooltips[selectedReligion])
    end
	self.gui.skills[1].OnMouseExit = function(object)
        hideTooltip(self)
    end

	self.gui.buttonHelp = GUI.Create("image", panel)
    self.gui.buttonHelp:SetImage("data/textures/gui/map/buttonHelp.png")
	self.gui.buttonHelp:SetPos(140, 30)
	self.gui.buttonHelp.OnMouseEnter = function(object)
        showTooltip(self, religionTooltips[selectedReligion])
    end
	self.gui.buttonHelp.OnMouseExit = function(object)
        hideTooltip(self)
    end

	self.gui.abilities = {}
	for i=1, 3 do
		local posX = 10
		local posY = 405 + 60 * (i - 1)
	
		self.gui.abilities[i] = {}
		local ability = self.gui.abilities[i]
		ability.panel = GUI.Create("image", panel)
		ability.panel:SetImage("data/textures/gui/map/Ability_" .. i .. ".png")
		ability.panel:SetPos(posX, posY)
		ability.panel.OnMouseEnter = function(object)
        	showTooltip(self, passiveTooltips[i])
	    end
		ability.panel.OnMouseExit = function(object)
	        hideTooltip(self)
	    end

		ability.cost = GUI.Create("text", ability.panel)
		ability.cost:SetPos(posX + 120, posY + 15)
		ability.cost:SetFont(self.gui.costFont)
		ability.cost:SetText({{color={1, 1, 0.4}}, "0"})

		local ap_passiveskills = GUI.Create("image", ability.panel)
		ap_passiveskills:SetImage("data/textures/gui/map/ap_passiveskills.png")
		ap_passiveskills:SetPos(posX + 105, posY + 20)

		ability.levels = {}
		for j=1, 4 do
			ability.levels[j] = GUI.Create("image", ability.panel)
			ability.levels[j]:SetImage("data/textures/gui/map/AbilityLvlDis.png")
			ability.levels[j]:SetPos(posX + 62 + (j - 1) * 21, posY + 35)
		end

		ability.button = GUI.Create("imagebutton", panel)
		ability.button:SetImage("data/textures/gui/map/buttonPlusDis.png")
    	ability.button:SizeToImage()
		ability.button:SetText("")
		ability.button:SetPos(posX + ability.panel:GetWidth() - 2, posY + 10)
		ability.button.OnClick = function(object, x, y)
			soundManager:PlaySound("click")
			TryBuyUpgrade(self, i)
		end
	end

    -- Кнопка старта
    local buttonPlayWidth = rightPanelWidth - 30
    self.gui.buttonPlay = GUI.Create("imagebutton", panel)
    self.gui.buttonPlay:SetImage("data/textures/gui/map/buttonStart-pass.png")
    self.gui.buttonPlay:SizeToImage()
    self.gui.buttonPlay:SetText("")
	self.gui.buttonPlay:SetPos(love.graphics:getWidth() - 220, love.graphics:getHeight() - 70)
	self.gui.buttonPlay.OnClick = function(object, x, y)
		if self.selectedLevel ~= "" then
			for i = 1, #regionsConfig do
				if regionsConfig[i].idName == self.selectedLevel then
					soundManager:PlaySound("click")
					GUI_None()
					self.game:StartLevel(regionsConfig[i].levelData)
					break
				end
			end
		end
	end

	self.gui.buttonPlay.OnMouseEnter = function(object)
        object:SetImage("data/textures/gui/map/buttonStart.png")
    end

	self.gui.buttonPlay.OnMouseExit = function(object)
        object:SetImage("data/textures/gui/map/buttonStart-pass.png")
    end
	
	
end

local function init(self)
	self.input:BindOnMouseDown(self, function()
		if self:GetInputManager():IsInputEnabled() then
			local worldPos = self:GetCamera():ScreenToWorld(self.input:GetMousePos())

			for i = 1, #self.regions do
				local region = self.regions[i]
				if region.levelProgressData.unlocked and not region.levelProgressData.completed and MapRegionCheckPixel(region, self.input:GetMousePos()) then
					region.underMouse = true
				end
			end
		end
	end)

	self.input:BindOnMouseMove(self, function(delta)
		if self:GetInputManager():IsInputEnabled() then
			
		end
	end)

	self.input:BindOnMouseUp(self, function()
		for i = 1, #self.regions do
			local region = self.regions[i]
			if region.underMouse and MapRegionCheckPixel(region, self.input:GetMousePos()) then
				SelectLevel(self, region.idName)
				soundManager:PlaySound("click")
			end
			region.underMouse = false
		end
	end)

	for i = 1, #regionsConfig do
		local regionConfig = regionsConfig[i]
		local region = MakeMapRegion(self.game, regionConfig.idName, regionConfig.pos, regionConfig.levelData, self.game.levelsProgressData[regionConfig.idName])
		region.zHeight = i

		self.world:AddActor(region)
		table.insert( self.regions, region)
	end
end

local function FindFirstOpenedLevel(self)
	for i=1, 8 do
		local progressData = self.game.levelsProgressData[tostring(i)]
		if progressData.unlocked and not progressData.completed then
			SelectLevel(self, tostring(i))
		end
	end
end

local function onopen(self)
	DebugLog('On Open MapScene')
	initGUI(self)
	FindFirstOpenedLevel(self)
end

function showMapPopup(self, text, finishCallback)
	self.waitForPopup = true
	disableGuiInput = true
		self.popupRawText = text
	self.popupText = love.graphics.newText(font2, self.popupRawText)
	self:GetInputManager():BindOnSomethingPressed(self, function()
		self.waitForPopup = false
		disableGuiInput = false
		self:GetInputManager():UnBindOnSomethingPressed(self)
		if finishCallback then
			finishCallback()
		end
	end, nil)
end

function MakeMapScene(game)
	local self = MakeScene(game)
	self:SetUpdateFn(update)
	self:SetPredrawFn(predraw)
	self:SetPostdrawFn(postdraw)
	self:SetPostguidrawFn(postguidraw)
	self:SetInitFn(init)
	self:SetOnOpenFn(onopen)

	self.regions = {}

	local texturePath = "data/textures/regions/" .. 'Back.png'

	self.popupRawText = "Get first position before time runs out to extend your cult to this region\nPress WASD to move"
	self.popupText = love.graphics.newText(font2, self.popupRawText)

    self.imageData = love.image.newImageData(texturePath)
	self.background = love.graphics.newImage(self.imageData)

	self.currentTooltip = nil
	self.tooltipDirectionUp = true
	self.waitForPopup = false

	return self
end
