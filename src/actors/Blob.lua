require "utils.Debug"
require "core.Point"
require "core.Actor"

BlobTypePlayer = 1
BlobTypeAiNeutral = 2
BlobTypeAiActive = 3
BlobTypeAiBoss = 4

FriendismAbility = 1
NeomagnitismAbility = 2
MirrorismAbility = 3

local function processAiMove(self, world, params, levelData)
	local move = MakePoint(0, 0)
	local ignoredMove = MakePoint(0, 0)

	local blobs = world:GetAllActorsWithTag("blob")
	for blob in pairs(blobs) do
		if blob ~= self then
			local distanceVec = blob.pos - self.pos
			local distance = (distanceVec):Size()
			local sizeDiff = self.radius / blob.radius

			local stepDirection = MakePoint(0, 0)

			-- ignore distant blobs
			if distance ~= 0 then
				if sizeDiff > 1 then
					stepDirection = distanceVec / (distance * distance)
				else
					stepDirection = -distanceVec / (distance * distance) * params.fearMultiplier
				end

				if distance > self.radius * params.distanceMultiplierIgnore then
					ignoredMove = ignoredMove + stepDirection * params.ignoredBlobPriority
				elseif sizeDiff > params.sizeDifferenceIgnore and distance - 5 > (self.radius + blob.radius) then
					ignoredMove = ignoredMove + stepDirection
				else
					move = move + stepDirection
				end
			end
		end
	end

	if move.x ~= 0 or move.y ~= 0 then
		return move:Ort()
	else
		if ignoredMove.x ~= 0 or ignoredMove.y ~= 0 then
			return ignoredMove:Ort();
		else
			return MakePoint(0, 0)
		end
	end
end


local function processUncontiousMove(self, world, params, levelData)
	local move = MakePoint(0, 0)

	local player = self.scene.playerBlob
	if player.radius > self.radius then
		local diff = player.pos - self.pos
		if diff:Size() < player.radius * params.neomagnetismRange then
			move = diff
		end
	end

	if move.x ~= 0 or move.y ~= 0 then
		return move:Ort() * params.neomagnetismVictimSpeed
	else
		return move
	end
end

local function update(self, dt)
	local params = game:GetParams()
	local im = game:GetInputManager()
	local world = self:GetWorld()
	local levelData = game:GetLevelData()
	local pastTime = self.scene.timeFull - self.scene.levelTime

	local move = MakePoint(0, 0)
	if self.type == BlobTypePlayer then
		move = MakePoint(im:GetXInput(), im:GetYInput()) * passiveAbilitySpreadSize
		if move.x ~= 0 or move.y ~= 0 then
			move = move:Ort()
		end
	else
		if self.type == BlobTypeAiActive then
			move = processAiMove(self, world, params, levelData)
		elseif selectedReligion == ReligionNeomagnetism then
			move = processUncontiousMove(self, world, params, levelData)
		end
	end

	local checkShrink = function(blob1, blob2)
		if blob1.type == BlobTypePlayer or blob2.type == BlobTypePlayer then
			soundManager:PlaySound("contactShort", true)
		end
	end

	self.pos = self.pos + move * params.heroSpeed * dt;

	if self.pos.x < 0 then
		self.pos.x = 0
	end

	if self.pos.x > levelData.size.x then
		self.pos.x = levelData.size.x
	end

	if self.pos.y < 0 then
		self.pos.y = 0
	end

	if self.pos.y > levelData.size.y then
		self.pos.y = levelData.size.y
	end

	-- collision
	if self.type ~= BlobTypeAiNeutral then
		local blobs = world:GetAllActorsWithTag("blob") -- neutrals can skip collision checks as they will be checked by others
		for blob in pairs(blobs) do
			if blob ~= self then
				local distance = (blob:GetPos() - self:GetPos()):Size()
				if distance < (self.radius + blob.radius) then
					if self.radius > blob.radius then
						local difference = blob.radius - (distance - self.radius)
						if self.type == BlobTypeAiActive and blob.type == BlobTypePlayer and blob.religionAbilities[FriendismAbility] then -- обработка религиозной пассивки Friendism
							self.radius = self.radius
							blob.radius = blob.radius - difference
						elseif self.type == BlobTypePlayer and self.religionAbilities[MirrorismAbility] and pastTime <= 10 then -- обработка религиозной пассивки Mirrorism
							if blob.type == BlobTypeAiNeutral then
								self.radius = self.radius + difference * blob.radius/self.radius * passiveAbilityInfluenceSize * 2
							else
								self.radius = self.radius + difference * blob.radius/self.radius * 2
							end
							blob.radius = blob.radius - difference
						elseif self.type == BlobTypePlayer and blob.type == BlobTypeAiNeutral then -- обработка 3 общей пассивки для всех религий
							self.radius = self.radius + difference * blob.radius/self.radius * passiveAbilityInfluenceSize
							blob.radius = blob.radius - difference
						else
							self.radius = self.radius + difference * blob.radius/self.radius
							blob.radius = blob.radius - difference
						end
						-- воспроизводим звук, если радиус съеденного чувака не сильно меньше нашего
						if self.radius/blob.radius < 5 then
							checkShrink(self, blob)
						end
					else
						local difference = self.radius - (distance - blob.radius)
						if self.type == BlobTypePlayer and blob.type == BlobTypeAiActive and self.religionAbilities[FriendismAbility] then -- обработка религиозной пассивки Friendism
							blob.radius = blob.radius
							self.radius = self.radius - difference
						elseif self.type == BlobTypePlayer and blob.religionAbilities[MirrorismAbility] and pastTime <= 10 then -- обработка религиозной пассивки Mirrorism
							if blob.type == BlobTypeAiNeutral then
								blob.radius = blob.radius + difference * self.radius/blob.radius * passiveAbilityInfluenceSize * 2
							else
								blob.radius = blob.radius + difference * self.radius/blob.radius * 2
							end
							self.radius = self.radius - difference
						else
							blob.radius = blob.radius + difference * self.radius/blob.radius
							self.radius = self.radius - difference
						end
						-- воспроизводим звук, если радиус съеденного чувака не сильно меньше нашего
						if blob.radius/self.radius < 5 then
							checkShrink(self, blob)
						end
					end
					if self.radius < params.minimumBlobSize then
						world:RemoveActor(self)
						self.onBlobDied(self)
						return
					end
					if blob.radius < params.minimumBlobSize then
						world:RemoveActor(blob)
						self.onBlobDied(blob)
						return
					end
				end
			end
		end
	end

	if self.type == BlobTypePlayer then
		local camera = game:GetMainCamera()
		-- local viewportSize = camera:GetViewportSize()
		local pos = self:GetPos()

		-- if pos.x - viewportSize.x/2 < 0 then
		-- 	pos.x = viewportSize.x/2
		-- end

		-- if pos.x + viewportSize.x/2 > levelData.size.x then
		-- 	pos.x = levelData.size.x - viewportSize.x/2
		-- end

		-- if pos.y - viewportSize.y/2 < 0 then
		-- 	pos.y = viewportSize.y/2
		-- end

		-- if pos.y + viewportSize.y/2 > levelData.size.y then
		-- 	pos.y = levelData.size.y - viewportSize.y/2
		-- end

		camera:SetZoom(params.zoomMultiplier * params.minimumZoomSize / math.max(math.floor(self.radius / params.zoomTreshold) * params.zoomTreshold, params.minimumZoomSize))

		camera:SetPos(pos)
	end
end

local function draw(self, shift, zoom)
	local posX = self.pos.x * zoom - self.radius * zoom + shift.x
	local posY = self.pos.y * zoom - self.radius * zoom + shift.y
	love.graphics.draw(self.circleTexture, posX, posY, 0, zoom * self.radius * 2 / self.circleTexture:getWidth(), zoom * self.radius * 2 / self.circleTexture:getHeight())
	if self.icon then
		local size = self.radius*zoom*game:GetParams().blobIconSize
		love.graphics.draw(self.icon, posX - size*0.5+self.radius*zoom, posY - size*0.5+self.radius*zoom, 0, size / self.icon:getWidth(), size / self.icon:getHeight())
	end
end

local texturePlayer = love.graphics.newImage("data/textures/blobs/player.png")
local textureNeutral = love.graphics.newImage("data/textures/blobs/neutral.png")
local textureActive = love.graphics.newImage("data/textures/blobs/active.png")

local religionTexture = {}
religionTexture["Friendism"] = love.graphics.newImage("data/textures/religionicons/Friendism.png")
religionTexture["Neomagnitism"] = love.graphics.newImage("data/textures/religionicons/Neomagnitism.png")
religionTexture["Mirrorism"] = love.graphics.newImage("data/textures/religionicons/Mirrorism.png")
religionTexture["Alien Liberators"] = love.graphics.newImage("data/textures/religionicons/Alien Liberators.png")
religionTexture["Bee Church"] = love.graphics.newImage("data/textures/religionicons/Bee Church.png")
religionTexture["Cat Worshipers"] = love.graphics.newImage("data/textures/religionicons/Cat Worshipers.png")
religionTexture["First Memologues"] = love.graphics.newImage("data/textures/religionicons/First Memologues.png")
religionTexture["Great Thinkers"] = love.graphics.newImage("data/textures/religionicons/Great Thinkers.png")
religionTexture["Hype Witnesses"] = love.graphics.newImage("data/textures/religionicons/Hype Witnesses.png")

function MakeBlob(game, pos, radius, type, name, onDie, scene)
	local self = MakeActor(game, pos)
	self:AddTag("blob")
	self:SetUpdateFn(update)
	self:SetDrawFn(draw)

	self.type = type
	self.radius = radius
	self.onBlobDied = onDie
	self.name = name
	self.scene = scene

	if type == BlobTypePlayer then
		self.circleTexture = texturePlayer
	elseif type == BlobTypeAiNeutral then
		self.circleTexture = textureNeutral
	else
		self.circleTexture = textureActive
	end

	if name and name ~= "" then
		self.icon = religionTexture[name]
	end

	self.religionAbilities = {}
	if type == BlobTypePlayer then
		self.religionAbilities[FriendismAbility] = selectedReligion == ReligionFriendism
		self.religionAbilities[NeomagnitismAbility] = selectedReligion == ReligionNeomagnetism
		self.religionAbilities[MirrorismAbility] = selectedReligion == ReligionMirrorism
	else
		self.religionAbilities[FriendismAbility] = false
		self.religionAbilities[NeomagnitismAbility] = false
		self.religionAbilities[MirrorismAbility] = false
	end

	return self
end
