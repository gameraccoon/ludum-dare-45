require "core.Point"
require "core.Actor"

local function update(self, dt)
	
end

local function draw(self, shift)
    if self.levelProgressData.completed then
        love.graphics.draw(self.textureIfNotHover, self.pos.x + shift.x, self.pos.y + shift.y)
    elseif self.levelProgressData.unlocked then
        love.graphics.draw(self.texturePassive, self.pos.x + shift.x, self.pos.y + shift.y)
    else
        local colorScale = 0.8
        love.graphics.setColor(colorScale, colorScale, colorScale, 1.0)
        love.graphics.draw(self.textureLocked, self.pos.x + shift.x, self.pos.y + shift.y)
        love.graphics.setColor(1.0, 1.0, 1.0, 1.0)

    end
    -- love.graphics.rectangle('line', self.pos.x, self.pos.y, self.texture:getWidth(), self.texture:getHeight())

end

local function postdrawfn(self, shift)
    if self.levelProgressData.unlocked then
        if self.selected then
            love.graphics.draw(self.textureActive, self.pos.x + shift.x, self.pos.y + shift.y - 2)
        end
    end
end

function MapRegionCheckPixel(reg, pos)
    local contains = false

    if pos.x > reg.pos.x and pos.x < reg.pos.x + reg.texturePassive:getWidth() and pos.y > reg.pos.y and pos.y < reg.pos.y + reg.texturePassive:getHeight() then
        texturePos = pos - reg.pos
        r,g,b,a = reg.imageData:getPixel(texturePos.x, texturePos.y)
        if a > 0.1 then
            contains = true
        end
    end
    
    return contains
end

function MakeMapRegion(game, idName, pos, levelData, levelProgressData)
    local self = MakeActor(game, pos)
	self:AddTag("map_region")
	self:SetUpdateFn(update)
    self:SetDrawFn(draw)
    self.postdraw = postdrawfn
    
    self.selected = false
    self.underMouse = false
    self.idName = idName
    self.levelData = levelData
    self.levelProgressData = levelProgressData

    local texturePath = "data/textures/regions/Passive/" .. idName .. '.png'
    local texturePathActive = "data/textures/regions/Active/" .. idName .. '.png'
    local texturePathLocked = "data/textures/regions/Locked/" .. idName .. '.png'
    local texturePathIfNotHover = "data/textures/regions/IfNotHover/" .. idName .. '.png'

    self.imageData = love.image.newImageData(texturePath)
    self.texturePassive = love.graphics.newImage(self.imageData)
    self.textureActive = love.graphics.newImage(texturePathActive)
    self.textureLocked = love.graphics.newImage(texturePathLocked)
    self.textureIfNotHover = love.graphics.newImage(texturePathIfNotHover)

	return self
end
