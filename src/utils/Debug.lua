-- for default
local debugMode = false

function IsDebugMode()
	return debugMode
end

function SetDebugMode(isEnable)
	if isEnable then
		print("====== DEBUG MODE IS ON ======")
	end

	debugMode = isEnable
end


-- behaviour depends on IsDebugMode()
function warning(message)
	if IsDebugMode() then
		error("[warning]: " .. message)
	else
		print("[warning]: " .. message)
	end
end

function DebugLog(message)
	if IsDebugMode() then
		print("[DEBUG] " .. message)
	end
end
