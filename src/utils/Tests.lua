require "utils.Debug"

-- array
local Tests = {}

function AddTest(name, fn)
	table.insert(Tests, {name = name, fn = fn})
end

function RunAllTests()
	print("Start tests")

	for i, test in ipairs(Tests) do
		if test and test.fn then
			test.fn(MakeChecker(test.name))
		else
			warning("broken test")
		end
	end

	print("All tests passed: " .. tostring(#Tests))
end


local Checker = {}
Checker.__index = Checker

function MakeChecker(name)
	local self = setmetatable({}, Checker)
	self.name = name
	return self
end

function Checker:TestEqual(expected, real)
	if expected ~= real then
		warning("Test " .. self.name .. " failed. Expected: \"" .. tostring(expected) .. "\" Real: \"" .. tostring(real) .. "\"")
	end
end

function Checker:TestEqualFloat(expected, real)
	if expected < real - 0.001 or expected > real + 0.001 then
		warning("Test " .. self.name .. " failed. Expected: \"" .. tostring(expected) .. "\" Real: \"" .. tostring(real) .. "\"")
	end
end

function Checker:TestTrue(condition)
	if not condition then
		warning("Test " .. self.name .. " failed. Expected: true Real: " .. tostring(condition))
	end
end

function Checker:TestFalse(condition)
	if condition then
		warning("Test " .. self.name .. " failed. Expected: false Real: " .. tostring(condition))
	end
end
