

function MakeLevelProgressData(unlocked, nextLevels)
    local levelProgressData = {}
    
    levelProgressData.unlocked = unlocked -- Открыт ли уровень?
    levelProgressData.completed = false -- Прошли ли этот уровень?
    levelProgressData.nextLevels = nextLevels -- Что следующее открываем, по прохождению
    levelProgressData.scores = 0 -- Сколько очков заработали

    levelProgressData.abilityFlock = 1
    levelProgressData.abilitySpread = 1
    levelProgressData.abilityInfluence = 1
    
	return levelProgressData
end