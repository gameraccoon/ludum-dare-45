require "utils.Tests"
require "utils.Debug"
require "core.Point"

local Camera = {}
Camera.__index = Camera

function MakeCamera(world, canvasSize)
	if not world then warning("world is nil") end

	local self = setmetatable({}, Camera)
	self.world = world
	self.pos = MakePoint(0, -100)
	self.frameSize = 20
	self.bgTexture = nil
	self.bgData = nil
	self.canvas = love.graphics.newCanvas(canvasSize.x, canvasSize.y)
	self.canvasSize = canvasSize
	self.cameraShift = MakePoint(0, -20)
	self.zoom = 1
	return self
end

function Camera:SetPos(pos)
	self.pos = pos:Floor()
end

function Camera:Shift(vector)
	self.pos = self.pos + vector
end

function Camera:SetZoom(zoom)
	self.zoom = zoom
end

function Camera:GetZoom()
	return self.zoom
end

function Camera:GetViewportSize()
	return MakePoint(love.graphics.getWidth(), love.graphics.getHeight())
end

function Camera:GetCanvasSize()
	return self.canvasSize:Clone()
end

function Camera:GetDrawShift()
	return (-self.pos * self.zoom + self.canvasSize * 0.5):Round()
end

function Camera:Follow(inPos)
	local pos = inPos + self.cameraShift
	self:SetPos(pos)
end

function Camera:ScreenToWorld(pos)
	local centerPos = pos - self:GetViewportSize() * 0.5
	return self.pos + centerPos	
end

function Camera:SetBg(bgData)
	self.bgData = bgData
	self.bgTexture = love.graphics.newImage(bgData.image)
end

function Camera:DrawBackgrounds()
	if self.bgTexture and self.bgData then
		local drawShift = self:GetDrawShift()
		local worldScreenCenter = MakePoint(love.graphics.getWidth()*0.5, love.graphics.getHeight()*0.5) - drawShift

		local start = worldScreenCenter - self.canvasSize*0.5
		local ending = worldScreenCenter + self.canvasSize*0.5
		start.x = math.floor(start.x / (self.bgTexture:getWidth() * self.zoom)) * self.bgTexture:getWidth() * self.zoom
		start.y = math.floor(start.y / (self.bgTexture:getHeight() * self.zoom)) * self.bgTexture:getHeight() * self.zoom

		for posX = start.x, ending.x, self.bgTexture:getWidth() * self.zoom do
			for posY = start.y, ending.y, self.bgTexture:getHeight() * self.zoom do
				love.graphics.draw(self.bgTexture, posX + drawShift.x, posY + drawShift.y, 0, self.zoom, self.zoom)
			end
		end
	end
end

function Camera:DrawStart()
	love.graphics.setCanvas(self.canvas)
	--love.graphics.setBackgroundColor(0, 0, 0)
	love.graphics.clear()
	love.graphics.setBlendMode("alpha")
	--love.graphics.setColor(255, 255, 255, 255)
end

function Camera:DrawWorld()
	local shift = self:GetDrawShift()
	self.world:ForeachActorInDrawOrder(function(actor)
		actor:Draw(shift, self.zoom)
	end)
end

function Camera:DrawEnd()
	love.graphics.setCanvas()
	love.graphics.setBlendMode("alpha", "premultiplied")
	love.graphics.draw(self.canvas, 0, 0, 0, love.graphics.getWidth() / self.canvasSize.x, love.graphics.getHeight() / self.canvasSize.y)
end


-------------------------------------------------------
---- Tests
-------------------------------------------------------

