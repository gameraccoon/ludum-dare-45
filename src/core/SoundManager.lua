local SoundManager = {}
SoundManager.__index = SoundManager

function MakeSoundManager()
	local self = setmetatable({}, SoundManager)
	self.sounds = {}
	return self
end

function SoundManager:GetSound(name)
	local s = self.sounds[name]
	if type(s) == "table" then
		local source = s[math.random(#s)]
		--love.audio.play(source)
		return source
	else
		--love.audio.play(s)
		return s
	end
end

function SoundManager:PlaySound(name, ignoreIfRepeated)
	local sound = self:GetSound(name)
	if not ignoreIfRepeated then
		love.audio.stop(sound)
	end
	love.audio.play(sound)
	return sound
end

function SoundManager:PlaySoundIndependent(name)
	local sound = self:GetSound(name):clone()
	love.audio.play(sound)
	return sound
end

function SoundManager:CloneSound(name)
	return self.GetSound(name):clone()
end

function SoundManager:StopSound(name)
	local s = self.sounds[name]
	if type(s) == "table" then
		for i,p in ipairs(s) do
			love.audio.stop(p)
		end
	else
		love.audio.stop(s)
	end
end

function SoundManager:SetVolume(name, volume)
	self:GetSound(name):setVolume(volume)
end

function SoundManager:SetDefaultVolume(name)
	local sound = self:GetSound(name)
	sound:setVolume(0.4)
end

function SoundManager:LoadAll()
	math.randomseed( os.time() )

    local soundsList = love.filesystem.load("data/sounds/list.lua")()
	
	for name, path in pairs(soundsList) do
		if type(path) == "table" then
			-- if several paths and/or parameters
			local list = {}
			local volume
			local loop = false
			for n,p in pairs(path) do
				if (n == "volume") then
					volume = p
				elseif (n == "loop") then
					loop = p
				else
					table.insert(list, love.audio.newSource("data/sounds/" .. p, "static"))
				end
			end
			
			-- set up parameters
			for i,p in ipairs(path) do
				list[i]:setLooping(loop)
				if type(volume) == "table" then
					list[i]:setVolume(volume[i])
				else
					list[i]:setVolume(volume)
				end
			end
			self.sounds[name] = list
		else
			-- if just name and path
			self.sounds[name] = love.audio.newSource("data/sounds/" .. path, "static")
		end
	end
end