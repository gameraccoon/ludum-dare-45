require "utils.Tests"
require "utils.Debug"
require "core.Camera"
require "core.World"
require "core.InputManager"

require "actors.Blob"
require "actors.MapRegion"

require "scenes.LogoScene"
require "scenes.CardSelectionScene"
require "scenes.GameplayScene"
require "scenes.MapScene"

require "data.LevelData"
require "data.LevelProgressData"

local Game = {}
Game.__index = Game

passiveAbilityFlockSize = 1
passiveAbilitySpreadSize = 1
passiveAbilityInfluenceSize = 1

function MakeGame(canvasSize)
	local self = setmetatable({}, Game)
	self.world = nil
	self.canvasSize = canvasSize

	self.params = {}
	self.params.heroSpeed = 100 -- скорость шарика
	self.params.heroInitialRadius = 10 -- начальный радиус шара игрока
	self.params.minimumBlobSize = 4 -- размер, при котором шар считается уничтоженным
	self.params.zoomMultiplier = 2 -- дефолтный зум на уровне
	self.params.minimumZoomSize = 20 -- минимальный размер шара, при котором зум отдаляется таким обазом, чтобы ты был такого же размера, как при дефолтном
	self.params.zoomTreshold = 20 -- шаг увеличения зума в зависимости от увеличения шара

	self.params.neomagnetismRange = 3 -- размер в радиусах героя
	self.params.neomagnetismVictimSpeed = 0.3 -- как быстро жертвы неомагнетизма идут к нам относительно обычной скорости

	self.params.afterGameDelaySec = 3

	-- scores
	self.params.placeToScore = {}
	self.params.placeToScore[1] = 200
	self.params.placeToScore[2] = 150
	self.params.placeToScore[3] = 80
	self.params.placeToScore[4] = 70
	self.params.placeToScore[5] = 50
	self.params.placeToScore[6] = 30
	self.params.placeToScore[7] = 20
	self.params.placeToScore[8] = 10
	
	self.params.multiplierFromLvl = {}
	self.params.multiplierFromLvl["1"] = 5
	self.params.multiplierFromLvl["2"] = 2
	self.params.multiplierFromLvl["3"] = 1
	self.params.multiplierFromLvl["4"] = 1
	self.params.multiplierFromLvl["5"] = 0.5
	self.params.multiplierFromLvl["6"] = 0.5
	self.params.multiplierFromLvl["7"] = 0.2
	self.params.multiplierFromLvl["8"] = 0.1

	self.params.uiScoreboardPos = MakePoint(10, 16)
	self.params.uiCenterTextPos = MakePoint(500, 240)
	self.params.uiTimerPos = MakePoint(177, 20)
	self.params.blobIconSize = 1.1
	self.params.introLength = 7

	-- AI configs
	self.params.fearMultiplier = 2 -- how is fear against bugger stronger than greed agains smaller
	self.params.distanceMultiplierIgnore = 20 -- distence in radiuses in which we ignore any blobs
	self.params.ignoredBlobPriority = 0.01 -- priority for ignored blobs
	self.params.sizeDifferenceIgnore = 10 -- ignore blobs that smaller us by this multiplier

	-- Прогрессы уровня
	self.levelsProgressData = {}
	self.levelsProgressData['1'] = MakeLevelProgressData(true, {'2'})
	self.levelsProgressData['2'] = MakeLevelProgressData(false, {'3'})
	self.levelsProgressData['3'] = MakeLevelProgressData(false, {'4'})
	self.levelsProgressData['4'] = MakeLevelProgressData(false, {'5'})
	self.levelsProgressData['5'] = MakeLevelProgressData(false, {'6'})
	self.levelsProgressData['6'] = MakeLevelProgressData(false, {'7'})
	self.levelsProgressData['7'] = MakeLevelProgressData(false, {'8'})
	self.levelsProgressData['8'] = MakeLevelProgressData(false, {})

	self.costsToUpgrade = {20, 40, 60, 80}

	self.scenes = {}
	self.scenes["logo"] = MakeLogoScene(self)
	self.scenes["gameplay"] = MakeGameplayScene(self)
	self.scenes["map"] = MakeMapScene(self)
	self.scenes["cardSelection"] = MakeCardSelectionScene(self)
	self.currentSceneName = ""
	self.isPaused = false

	return self
end

function Game:UnlockNextLevelsByCompleted(completedLevel)
	self.levelsProgressData[completedLevel].completed = true
	local nextLevels = self.levelsProgressData[completedLevel].nextLevels
	for i = 1, #nextLevels do
		self.levelsProgressData[nextLevels[i]].unlocked = true
	end
	if #nextLevels ~= 0 then
		showMapPopup(self.scenes["map"], "Congratulations! You have captured this region.\nNow one of the cult followers goes to the next\nregion to start there with nothing.")
	else
		showMapPopup(self.scenes["map"], "Excellent! You were able to spread your cult to all world!\nYou are the Supreme lord!\nTry to do it with another cult.", function()
			DebugLog('Restart Game')
			GUI_None()
			game:Init()
			game:StartGame()
		end)
	end
end

function Game:SetScene(sceneName)
	if not sceneName then warning("sceneName is nil"); sceneName="map" end

	DebugLog("set scene: " .. sceneName)

	self.currentSceneName = sceneName
	self.world = self.scenes[sceneName]:GetWorld()
	if self.scenes[sceneName].onOpenFn then
		self.scenes[sceneName].onOpenFn(self.scenes[sceneName])
	end
end

function Game:StartLevel(levelData)
	self.scenes["gameplay"] = MakeGameplayScene(self)
	self:SetScene("gameplay")
	self:GetCurrentScene():StartLevel(levelData)

	passiveAbilityFlockSize = self.levelsProgressData[levelData.levelId].abilityFlock
	passiveAbilitySpreadSize = self.levelsProgressData[levelData.levelId].abilitySpread
	passiveAbilityInfluenceSize = self.levelsProgressData[levelData.levelId].abilityInfluence

	DebugLog("flock " .. passiveAbilityFlockSize .. " spread " .. passiveAbilitySpreadSize .. " influence " .. passiveAbilityInfluenceSize)
end

function Game:GetLevelData()
	return self.scenes["gameplay"]:GetLevelData()
end

function Game:GetCurrentScene()
	return self.scenes[self.currentSceneName]
end

function Game:GetMainCamera()
	return self:GetCurrentScene():GetCamera()
end

function Game:GetWorld()
	return self.world
end

function Game:GetParams()
	return self.params
end

function Game:GetInputManager()
	return self:GetCurrentScene():GetInputManager()
end

function Game:IsPaused()
	return self.isPaused
end

function Game:Pause()
	self.isPaused = true
end

function Game:Unpause()
	self.isPaused = false
end

function Game:Update(dt)
	self:GetCurrentScene():Update(dt)
	if self.world and not self:IsPaused() then
		self.world:ForeachActor(function(actor)
			actor:Update(dt)
		end)
	end
end

function Game:Init()
	for _, scene in pairs(self.scenes) do
		scene:Init()
	end
end

function Game:OpenMap()
	soundManager:SetDefaultVolume("bgMusic")
	self:Unpause()
	self:SetScene("map")
	self:GetMainCamera():SetPos(MakePoint(love.graphics.getWidth() / 2, love.graphics.getHeight() / 2))
end

function Game:StartGame()
	soundManager:PlaySound("bgMusic")
	self:Unpause()
	self:SetScene("logo")
end

function Game:WinLevel(levelId)
	self:UnlockNextLevelsByCompleted(levelId)
	self:Unpause()
	self:OpenMap()
end

function Game:LooseLevel(levelId, score)
	self.levelsProgressData[levelId].scores = self.levelsProgressData[levelId].scores + score
	self:Unpause()
	self:OpenMap()
end
