require "utils.Tests"
require "utils.Debug"
require "core.Point"

local InputManager = {}
InputManager.__index = InputManager

function MakeInputManager()
	local self = setmetatable({}, InputManager)
	self.scancodeMap = {}
	self.mouseDownBindings = {}
	self.mouseUpBindings = {}
	self.mouseMoveBindings = {}
	self.somethingPressBindings = {}
	self.isMouseDown = false
	self.lastMousePos = MakePoint(0, 0)
	self.isInputEnabled = true
	return self
end

function InputManager:BindScancode(object, scancode, fnPressed, fnReleased)
	local scData = self.scancodeMap[scancode]
	
	if not scData then
		scData = {}
		scData.isDown = self:IsScancodeDown(scancode) 
		scData.fns = {}
	end

	scData.fns[object] = {pressed=fnPressed, released=fnReleased}

	self.scancodeMap[scancode] = scData

	if scData.isDown then
		fnPressed()
	end
end

function InputManager:UnbindScancode(object, scancode)
	local scData = self.scancodeMap[scancode]

	if not scData then
		return
	end

	scData.fns[object] = nil
end

function InputManager:BindOnMouseDown(object, fn)
	self.mouseDownBindings[object] = fn

	if self:IsMouseDown() then
		fn()
	end
end

function InputManager:UnBindOnMouseDown(object)
	self.mouseDownBindings[object] = nil
end

function InputManager:BindOnMouseUp(object, fn)
	self.mouseUpBindings[object] = fn
end

function InputManager:UnBindOnMouseUp(object)
	self.mouseUpBindings[object] = nil
end

function InputManager:BindOnMouseMove(object, fn)
	self.mouseMoveBindings[object] = fn
end

function InputManager:UnBindOnMouseMove(object)
	self.mouseMoveBindings[object] = nil
end

function InputManager:UnbindObject(object)
	for _, scData in pairs(self.scancodeMap) do
		scData.fns[object] = nil
	end

	self.mouseMoveBindings[object] = nil
	self.mouseDownBindings[object] = nil
	self.mouseUpBindings[object] = nil
	self.somethingPressBindings[object] = nil
end

function InputManager:IsMouseDown()
	return love.mouse.isDown(1)
end

function InputManager:GetMousePos()
	local x, y = love.mouse.getPosition()
	return MakePoint(x, y)
end

function InputManager:BindOnSomethingPressed(object, fn)
	self.somethingPressBindings[object] = fn
end

function InputManager:UnBindOnSomethingPressed(object)
	self.somethingPressBindings[object] = nil
end

function InputManager:OnSomethingPressed()
	for _, fn in pairs(self.somethingPressBindings) do
		fn()
	end
end

function InputManager:IsScancodeDown(scancode)
	return love.keyboard.isScancodeDown(scancode)
end

function InputManager:CheckScancodes()
	for scancode, scData in pairs(self.scancodeMap) do
		local isKeyDown = self:IsScancodeDown(scancode)
		if not scData.isDown and isKeyDown then
			scData.isDown = true
			for _, fn in pairs(scData.fns) do
				if fn.pressed then
					fn.pressed()
				end
			end
			self:OnSomethingPressed()
		end
		
		if scData.isDown and not isKeyDown then
			scData.isDown = false
			for _, fn in pairs(scData.fns) do
				if fn.released then
					fn.released()
				end
			end
		end
	end
end

function InputManager:OnMouseMove(delta)
	for _, fn in pairs(self.mouseMoveBindings) do
		fn(delta)
	end
end

function InputManager:CheckMouse()
	local isMouseDown = self:IsMouseDown()
	if isMouseDown and not self.isMouseDown then
		self.isMouseDown = true
		for _, fn in pairs(self.mouseDownBindings) do
			fn()
		end
		self:OnSomethingPressed()
	elseif not isMouseDown and self.isMouseDown then
		self.isMouseDown = false
		self.lastMousePos = self:GetMousePos()
		for _, fn in pairs(self.mouseUpBindings) do
			fn()
		end
	elseif isMouseDown and self.isMouseDown then
		local mousePos = self:GetMousePos()
		if mousePos ~= self.lastMousePos then
			self:OnMouseMove(mousePos - self.lastMousePos)
			self.lastMousePos = mousePos
		end
	end
end

function InputManager:Update(dt)
	self:CheckScancodes()
	self:CheckMouse()
end

function InputManager:GetXInput()
	local result = 0

	if self:IsScancodeDown("d") or self:IsScancodeDown("right") then
		result = result + 1
	end

	if self:IsScancodeDown("a") or self:IsScancodeDown("left") then
		result = result - 1
	end

	return result
end

function InputManager:GetYInput()
	local result = 0

	if self:IsScancodeDown("w") or self:IsScancodeDown("up") then
		result = result - 1
	end

	if self:IsScancodeDown("s") or self:IsScancodeDown("down") then
		result = result + 1
	end

	return result
end

function InputManager:SetInputEnabled(isEnabled)
	self.isInputEnabled = isEnabled
end

function InputManager:IsInputEnabled()
	return self.isInputEnabled
end

-------------------------------------------------------
---- Tests
-------------------------------------------------------

AddTest("InputManager-Bind", function(checker)
	local im = MakeInputManager()

	local isKeyDown = true

	im.IsScancodeDown = function(self)
			return isKeyDown
		end

	local isMouseDown = true
	local mousePos = MakePoint(0, 0)
	im.IsMouseDown = function(self)
			return isMouseDown
		end
	im.GetMousePos = function(self)
			return mousePos
		end

	local objectDummy = 123 -- not actually an object but this should work

	-- ===== bind scancode
	local testVal = 1
	im:BindScancode(objectDummy, "test",
		function() -- pressed
			testVal = 3
		end,
		function() -- released
			testVal = 4
		end)

	checker:TestEqual(3, testVal)

	isKeyDown = false
	im:Update(0.0)

	checker:TestEqual(4, testVal)

	isKeyDown = true
	im:Update(0.0)
	checker:TestEqual(3, testVal)
	
	-- ===== bind mouse
	local mouseVal1 = 0
	local mouseVal2 = 0
	local mouseVal3 = 0
	im:BindOnMouseDown(objectDummy, function()
			mouseVal1 = mouseVal1 + 1	
		end)
	im:BindOnMouseUp(objectDummy, function()
			mouseVal2 = mouseVal2 + 1	
		end)
	im:BindOnMouseMove(objectDummy, function(delta)
			mouseVal3 = mouseVal3 + delta.x + delta.y	
		end)

	checker:TestEqual(1, mouseVal1)
	checker:TestEqual(0, mouseVal2)
	checker:TestEqual(0, mouseVal3)

	isMouseDown = true
	im:Update(0.0)

	checker:TestEqual(1, mouseVal1)
	checker:TestEqual(0, mouseVal2)
	checker:TestEqual(0, mouseVal3)
		
	mousePos = MakePoint(10, 10)
	im:Update(0.0)

	checker:TestEqual(1, mouseVal1)
	checker:TestEqual(0, mouseVal2)
	checker:TestEqual(20, mouseVal3)
	
	isMouseDown = false
	im:Update(0.0)
	
	checker:TestEqual(1, mouseVal1)
	checker:TestEqual(1, mouseVal2)
	checker:TestEqual(20, mouseVal3)

	mousePos = MakePoint(20, 20)
	im:Update(0.0)

	checker:TestEqual(1, mouseVal1)
	checker:TestEqual(1, mouseVal2)
	checker:TestEqual(20, mouseVal3)
end)

AddTest("InputManager-UnBind", function(checker)
	local im = MakeInputManager()

	local isDown = false

	im.IsScancodeDown = function(self)
			return isDown
		end

	im.IsMouseDown = function(self)
			return false
		end

	local objectDummy = 123 -- not actually an object but this should work

	local testVal = 1
	im:BindScancode(objectDummy, "test",
		function() -- pressed
			testVal = 3
		end,
		function() -- released
			testVal = 4
		end)

	im:UnbindScancode(objectDummy, "test")

	isDown = true
	im:Update(0.0)
	checker:TestEqual(1, testVal)

	isDown = false
	im:Update(0.0)
	checker:TestEqual(1, testVal)
end)

AddTest("InputManager-UnBindObject", function(checker)
	local im = MakeInputManager()

	local isDown = false

	im.IsScancodeDown = function(self)
			return isDown
		end

	im.IsMouseDown = function(self)
			return false
		end

	local objectDummy = 123 -- not actually an object but this should work

	local testVal = 1
	im:BindScancode(objectDummy, "test",
		function() -- pressed
			testVal = 3
		end,
		function() -- released
			testVal = 4
		end)

	im:UnbindObject(objectDummy)

	isDown = true
	im:Update(0.0)
	checker:TestEqual(1, testVal)

	isDown = false
	im:Update(0.0)
	checker:TestEqual(1, testVal)
end)
