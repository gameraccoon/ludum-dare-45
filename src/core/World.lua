require "utils.Tests"
require "utils.Debug"


local World = {}
World.__index = World

function MakeWorld()
	local self = setmetatable({}, World)
	self.actors = {}
	self.tagMap = {}
	self.drawLayersMap = {}
	self.drawLayers = {}
	self.colliderLayer = nil
	return self
end

function World:BindActorTags(actor)
	for tag in pairs(actor:GetTags()) do
		if not self.tagMap[tag] then
			self.tagMap[tag] = {}
		end

		self.tagMap[tag][actor] = true
	end
end

function World:UnbindActorTags(actor)
	for tag in pairs(actor:GetTags()) do
		self.tagMap[tag][actor] = nil
	end
end

function World:AddActorToLayersMap(actor)
	local zHeight = actor:GetZHeight()
	if not self.drawLayersMap[zHeight] then
		self.drawLayersMap[zHeight] = {}
		table.insert(self.drawLayers, zHeight)
		table.sort(self.drawLayers)
	end
	self.drawLayersMap[actor:GetZHeight()][actor] = true
end

function World:RemoveActorFromLayersMap(actor)
	self.drawLayersMap[actor:GetZHeight()][actor] = nil
end

function World:AddActor(actor)
	self.actors[actor] = true

	self:BindActorTags(actor)
	self:AddActorToLayersMap(actor)

	actor:BeginPlay()
end

function World:RemoveActor(actor)
	if self.actors[actor] then
		actor:OnDestruct()
	end

	self:UnbindActorTags(actor)
	self:RemoveActorFromLayersMap(actor)

	self.actors[actor] = nil
end

function World:GetAllActorsWithTag(tag)
	local actors = self.tagMap[tag]

	if actors then
		return actors
	else
		return {}
	end
end

function World:ForeachActor(fn)
	if not fn then warning("fn is nil") end

	for actor, _ in pairs(self.actors) do
		fn(actor)
	end
end

function World:ForeachActorInDrawOrder(fn)
	if not fn then warning("fn is nil") end

	for _, layerId in pairs(self.drawLayers) do
		layerMap = self.drawLayersMap[layerId]
		if layerMap then
			for actor in pairs(layerMap) do
				fn(actor)
			end
		end
	end
end

