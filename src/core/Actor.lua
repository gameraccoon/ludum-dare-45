require "core.Point"


local Actor = {}
Actor.__index = Actor

function MakeActor(game, pos)
	if not game then warning("game is nil") end
	if not pos then warning("pos is nil") end

	local self = setmetatable({}, Actor)
	self.pos = pos
	self.game = game
	self.updateFn = nil
	self.drawFn = nil
	self.beginPlayFn = nil
	self.onDestructFn = nil
	self.tags = {}
	self.isSpawned = false
	self.isDestructed = false
	self.zHeight = 0
	return self
end

function Actor:SetPos(pos)
	self.pos = pos:Clone()
end

function Actor:GetPos()
	return self.pos:Clone()
end

function Actor:GetZHeight()
	return self.zHeight
end

function Actor:AddTag(tag)
	if self.isSpawned then
		warning("tags shouldn't be changed after construction")
		return
	end

	self.tags[tag] = true
end

function Actor:RemoveTag(tag)
	if self.isSpawned then
		warning("tags shouldn't be changed after construction")
		return
	end

	self.tags[tag] = nil
end

function Actor:HasTag(tag)
	return self.tags[tag] ~= nil
end

function Actor:GetTags()
	return self.tags
end

function Actor:Update(dt)
	if self.updateFn then
		self.updateFn(self, dt)
	end
end

function Actor:Draw(shift, zoom)
	if self.drawFn then
		self.drawFn(self, shift, zoom)
	end
end

-- called once after construction
function Actor:BeginPlay()
	self.isSpawned = true
	if self.beginPlayFn then
		self.beginPlayFn(self)
	end
end

-- called before removing from the world
function Actor:OnDestruct()
	if self.onDestructFn then
		self.onDestructFn(self)
	end
	self.isDestructed = true
end

function Actor:IsValid()
	return self.isSpawned and not self.isDestructed
end

function Actor:SetUpdateFn(fn)
	if not fn then warning("fn is nil"); return end

	if self.updateFn then
		local oldUpdateFn = self.updateFn
		self.updateFn = function(self, dt)
			oldUpdateFn(self, dt)
			fn(self, dt)
		end
	else
		self.updateFn = fn
	end
end

function Actor:SetDrawFn(fn)
	if not fn then warning("fn is nil"); return end

	if self.drawFn then
		local oldDrawFn = self.drawFn
		self.drawFn = function(self, shift, zoom)
			oldDrawFn(self, shift, zoom)
			fn(self, shift, zoom)
		end
	else
		self.drawFn = fn
	end
end

function Actor:SetBeginPlayFn(fn)
	if not fn then warning("fn is nil"); return end

	if self.beginPlayFn then
		local oldFn = self.beginPlayFn
		self.beginPlayFn = function(self)
			oldFn(self)
			fn(self)
		end
	else
		self.beginPlayFn = fn
	end
end

function Actor:SetOnDestructFn(fn)
	if not fn then warning("fn is nil"); return end

	if self.onDestructFn then
		local oldFn = self.onDestructFn
		self.onDestructFn = function(self)
			oldFn(self)
			fn(self)
		end
	else
		self.onDestructFn = fn
	end
end

function Actor:GetWorld()
	if not self.game then warning("self.game is nil"); return end

	return self.game:GetWorld()
end

function Actor:GetGame()
	return self.game
end
