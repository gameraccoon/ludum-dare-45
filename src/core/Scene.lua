require "utils.Debug"
require "core.World"

local Scene = {}
Scene.__index = Scene

function MakeScene(game)
	local self = setmetatable({}, Scene)
	self.game = game
	self.world = MakeWorld()
	self.camera = MakeCamera(self.world, MakePoint(love.graphics.getWidth(), love.graphics.getHeight()))
	self.input = MakeInputManager()
	return self
end

function Scene:GetWorld()
	return self.world
end

function Scene:GetGame()
	return self.game
end

function Scene:GetCamera()
	return self.camera
end

function Scene:GetInputManager()
	return self.input
end

function Scene:SetOnOpenFn(fn)
	if not fn then warning("fn is nil"); return end

	if self.onOpenFn then
		local oldUpdateFn = self.onOpenFn
		self.onOpenFn = function(self, dt)
			oldUpdateFn(self, dt)
			fn(self, dt)
		end
	else
		self.onOpenFn = fn
	end
end

function Scene:SetUpdateFn(fn)
	if not fn then warning("fn is nil"); return end

	self.updateFn = fn
end

function Scene:SetInitFn(fn)
	if not fn then warning("fn is nil"); return end

	if self.initFn then
		local oldInitFn = self.initFn
		self.initFn = function(self, dt)
			oldInitFn(self, dt)
			fn(self, dt)
		end
	else
		self.initFn = fn
	end
end

function Scene:SetPredrawFn(fn)
	if not fn then warning("fn is nil"); return end

	if self.predrawFn then
		local oldpredrawFn = self.predrawFn
		self.predrawFn = function(self)
			oldpredrawFn(self)
			fn(self)
		end
	else
		self.predrawFn = fn
	end
end

function Scene:SetPostdrawFn(fn)
	if not fn then warning("fn is nil"); return end

	if self.postdrawFn then
		local oldpostdrawFn = self.postdrawFn
		self.postdrawFn = function(self)
			oldpostdrawFn(self)
			fn(self)
		end
	else
		self.postdrawFn = fn
	end
end

function Scene:SetPostguidrawFn(fn)
	if not fn then warning("fn is nil"); return end

	if self.postguidrawFn then
		local oldpostdrawFn = self.postguidrawFn
		self.postguidrawFn = function(self)
			oldpostdrawFn(self)
			fn(self)
		end
	else
		self.postguidrawFn = fn
	end
end

function Scene:Update(dt)
	if self.input then
		self.input:Update(dt)
	else
		warning("self.input is nil")
	end

	if self.updateFn then
		self.updateFn(self, dt)
	end
end

function Scene:Draw()
	self.camera:DrawStart()
	self.camera:DrawBackgrounds()

	if self.predrawFn then
		self.predrawFn(self)
	end

	self.camera:DrawWorld()

	if self.postdrawFn then
		self.postdrawFn(self)
	end

	self.camera:DrawEnd()
end

function Scene:PostGuiDraw()
	if self.postguidrawFn then
		self.postguidrawFn(self)
	end
end

function Scene:Init()
	if self.initFn then
		self.initFn(self)
	end
end
